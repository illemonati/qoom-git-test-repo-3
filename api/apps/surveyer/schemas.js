const Configs = require('../../../config.js')
;

const configs = Configs()
	, dbUri = configs.MONGODB_URI
;

function getSurveySchema(mongooseModule) { 
	let surveySchema =  new mongooseModule.Schema({
		name: String
		, domain: String
		, results: {}
		, backupId: String
	}, {usePushEach: true, collection: 'surveys'})

	return surveySchema;
}

module.exports = {
	survey: getSurveySchema
	, dbUri 
}