const 
	Configs = require('../../../config.js')
	, fs = require('fs')
	, async = require('async')
	, multiparty = require('multiparty')
	, path = require('path')
	, parser = require('ua-parser-js')
;

const 
	configs = Configs()
	, cache = {}
;

let
	appName
	, authenticater, helper, saver, surveyer, schemas, administrater, renderer, restricter
;

function initialize() {
	authenticater = require('../authenticater/api.js');
	helper = require('../helper/app.js');
	restricter = require('../restricter/app.js');
	surveyer = require('./app.js');
	renderer = require('../renderer/app.js');
	schemas = require('./schemas.js');
	administrater = require('../administrater/app.js');
	saver = require('../saver/app.js');
	appName = surveyer.appName;
	surveyer.initialize();
	saver.initialize();
	renderer.initialize();
	supportedFileTypes = renderer.getSupportedFileTypes();
}

function isValidPerson(req) {
	return !!(req.person && req.passcodeInCookieMatched && req.person.ship && req.person.ship.name);
}

function addRoutes(app) {
	if(!configs.email || !configs.email.enableApi) return;
	
	app.get(`/${appName}/section`, (req, res, next) => {
		res.contentType('text/html');
		if(!isValidPerson(req)) {
			return res.redirect(administrater.loginPath);
		}

		cache.sectionCSS = fs.readFileSync(path.join(__dirname, '../../libs/surveyer/src/css/section.css'), 'utf8');
		cache.sectionJS = fs.readFileSync(path.join(__dirname, '../../libs/surveyer/src/js/section.js'), 'utf8');
		cache.sectionHTML = fs.readFileSync(path.join(__dirname, '../../libs/surveyer/src/html/section.html'), 'utf8');
		
		surveyer.getSurveys({domain: req.headers.host}, null, (err, surveys) => {
			const dataToBind = {
				baseCSS: administrater.getBaseCSS()
				, baseJS: administrater.getBaseJS()
				, sectionCSS: cache.sectionCSS
				, sectionJS: cache.sectionJS
				, surveys: surveys.map(s => `<button>${s}</button>`)
			}
			
			const items = administrater.getMenuUrls(req.person.services);
			helper.injectWidgets(cache.sectionHTML, dataToBind, [
				{loader: administrater.getMenuWidget({items}), placeholder: 'menu'}
				, {loader: administrater.getHeaderWidget({name: 'Survey Explorer'}), placeholder: 'header'}
				, {loader: administrater.getFooterWidget({}), placeholder: 'footer'}
				], (err, sectionPage) => {
					if(err) return next({status: 500, error: err})
					res.send(sectionPage);
				})			
		})
	});
	
	app.get(`/${appName}/report`, (req, res, next) => {
		res.contentType('text/html');
		if(!isValidPerson(req)) {
			return res.redirect(administrater.loginPath);
		}
		
		const { survey } = req.query
		; 
		
		if(!survey) return res.send('No survey provided in query string')
		
		cache.tableCSS = cache.tableCSS || fs.readFileSync(path.join(__dirname, '../../libs/surveyer/src/css/table.css'), 'utf8');
		cache.tableJS = cache.tableJS || fs.readFileSync(path.join(__dirname, '../../libs/surveyer/src/js/table.js'), 'utf8');
		cache.tableHTML = cache.tableHTML || fs.readFileSync(path.join(__dirname, '../../libs/surveyer/src/html/table.html'), 'utf8');
		
		const dataToBind = {
			baseCSS: administrater.getBaseCSS()
			, baseJS: administrater.getBaseJS()
			, tableCSS: cache.tableCSS
			, tableJS: cache.tableJS
		}
		
		const items = administrater.getMenuUrls(req.person.services);
		helper.injectWidgets(cache.tableHTML, dataToBind, [
			{loader: administrater.getMenuWidget({items}), placeholder: 'menu'}
			, {loader: administrater.getHeaderWidget({name: `${helper.capitalizeFirstLetter(survey)}`}), placeholder: 'header'}
			, {loader: administrater.getFooterWidget({}), placeholder: 'footer'}
			, {loader: surveyer.getTableWidget({table: survey, domain: req.headers.host}), placeholder: 'table'}
			], (err, tablePage) => {
				if(err) return next({status: 500, error: err})
				res.send(tablePage);
			})
	});


	app.delete(`/${appName}/:id([0-9a-f]{24})`, (req, res, next) => {
		res.contentType('application/json');
		if(!isValidPerson(req)) return next({status: 401, error: 'Not Authenticated' });
		
		const { id } = req.params;
		
		saver.schemaUpdate(
		{	
			schemaName: 'survey'
			, collectionName: 'Survey'
			, schema: schemas.survey
			, modelData: { $set: { backupId: id}}
			, backup: false
			, dbUri: schemas.dbUri
			, _id: id
		}
		, function() {}
		, (err) => {
			if(err) return res.send({status: 500, error: err});
			res.send({success: true});
		});
	})

	app.post(`/${appName}/submit`, (req, res, next) => {
		res.contentType('application/json');
		if(!req.body) return next({status: 400, error: 'Missing body' });
		if(!req.body.email) return next({status: 400, error: 'Missing email' });
		if(!req.body.email.to) return next({status: 400, error: 'Missing to' });
		if(!req.body.email.template) return next({status: 400, error: 'Missing template' });
		if(!req.body.survey) return next({status: 400, error: 'Missing survey' });
		if(!req.body.survey.survey) return next({status: 400, error: 'Missing survey name' });
		
		const options = { 
				email: {
					to: req.body.email.to ? req.body.email.to.split(',') : undefined
					, bcc: req.body.email.bcc ? req.body.email.bcc.split(',') : undefined
					, cc: req.body.email.cc ? req.body.email.cc.split(',') : undefined
					, subject: req.body.email.subject
					, template: { name: req.body.email.template, data: req.body.survey }
					, from: req.body.email.from || `no_reply@${req.headers.host.indexOf(':') > -1 ? 'wisen.space' : req.headers.host}`
				},
				requestDomain: req.headers.host,
				survey: req.body.survey
			}
		;
		surveyer.saveAndSend(options, null, (err) => {
			if(err) return next({status: 500, error: err});
			res.send({success: true});
		});
	});
	
	app.post(`/${appName}/contactus`, (req, res, next) => {
		res.contentType('application/json');
		if(!req.body) return next({status: 400, error: 'Missing body' });
		if(!req.body.survey) return next({status: 400, error: 'Missing survey' });
		if(!req.body.survey.survey) return next({status: 400, error: 'Missing survey name' });
		
		const options = {
				email: {
					to: ['hello@qoom.io']
					, subject: 'Contact Request'
					, template: { name: 'lander/contactus.email', data: req.body.survey }
					, from: 'hello@qoom.io'
				},
				requestDomain: req.headers.host,
				survey: req.body.survey
			}
		;
		surveyer.saveAndSend(options, null, (err) => {
			if(err) return next({status: 500, error: err});
			res.send({success: true});
		});

	});
	
	app.post(`/${appName}/checkout`, async (req, res, next) => {
		const form = new multiparty.Form({
			maxFilesSize: 4000000000
		});
		
		let survey, email, requestDomain = req.headers.host;
		
		const processFile = async (file, surveyName) => {
			
			// console.log('survey_files/', surveyName.replace('/', '_')+'/', file.fieldName.replace('.', '_'), file.originalFileName);
			// console.log(file)
			
            let parsedFileName = path.join(
            	'survey_files/', 
            	surveyName.replace('/', '_').replace(' ', '_')+'/', 
            	file.fieldName.replace('.', '_').replace(' ', '_')+'/', 
            	file.originalFilename
        	);
            

            const ext = parsedFileName.split('.').reverse()[0].toLowerCase();
            const renderFileDefaultText = '';

            const encoding = renderer.getEncoding(ext); 
            const fileContents = encoding === 'binary' ? fs.readFileSync(file.path) : fs.readFileSync(file.path, 'utf8');

            if (!fileContents && ext && supportedFileTypes[ext]) {
                renderFileDefaultText =
                    supportedFileTypes[ext].defaultText || '';
                fileContents = renderFileDefaultText;
            }

            if (!parsedFileName) return;
            if (parsedFileName.startsWith('/'))
                parsedFileName = parsedFileName.slice(1);
            if (parsedFileName.endsWith('/')) {
                parsedFileName = parsedFileName + '__hidden';
                fileContents = 'This is hidden file.';
            }
            const isBackend = /\.api$|\.schemas$|\.app$/.test(parsedFileName);
            
            if (isBackend) return;
            
            const saverOptions = {
                file: parsedFileName,
                domain: req.headers.host,
                allowBlank: true,
                encoding,
                data: fileContents,
                title: parsedFileName.replace(/^.*[\\\/]/, ''),
                updateFile: !isBackend,
                backup: true,
            };

            const restrictions = restricter.getRestrictedFiles();
            if (restrictions.includes(parsedFileName))
                handleError('restricted file', 400);

			try {
	            await new Promise((res, rej) => {
	            	saver.update(saverOptions, (err) => {
	            		if (err) {rej(err)} else {res()};
	            	})
            	});
            	return parsedFileName;
			} catch(e){}
			
        };
		
		function parseForm(next) {
			form.parse(req, async function(err, fields, files) {
				if(err) return next(err);
				survey = Object.keys(fields).reduce((o, k) => {
					o[k] = (fields[k] && fields[k].length && fields[k].length === 1)
						? fields[k].toString() 
						: (fields[k] && fields[k][0])
					return o;
				}, {})
				
				if(fields.email && fields.email.length) {
					email = {
						to: fields.email
						, subject: survey.subject || 'Contact Request'
						, template: survey.template
							? { name: survey.template, data: survey }
							: ''
						, from: configs && configs.emailer && configs.emailer.from && 'Qoom <hello@qoom.io>'
					}
				}
				
				survey['attachments'] = {};
				for (fileName in files) {
					console.log(files[fileName]);
					const savedFile = await processFile(files[fileName][0], survey.survey);
					if (savedFile) {
						console.log(savedFile);
						survey['attachments'][fileName.replace('.', '_')] = savedFile;
					}
				}
				next();
			});			
		}
		
		function getAmount(next) {
			if(!survey.token) return next();
			const filename = helper.getFileNameFromReferrer(req, '', true);
			if(filename.startsWith('/')) filename = filename.slice(1);
			saver.load({
				file: filename
				, domain: requestDomain
			}, (err, fileData) => {
				if(err) return next(err);
				const match = fileData.match(/!!\$\s(.*)/);
				if(!match || !match[1]) return next('No amount found');
				survey.amount = parseFloat(match[1]);
				console.log(survey);
				if(!survey.amount || isNaN(survey.amount)) return next('No amount applied');
				next();
				
			});
		}
		
		function purchase(next) {
			if(!survey.token) return next();
			try {
				const transacter = require('../transacter/app.js');
				console.log({
					amount: survey.amount
					, token: survey.token
					, metadata: {survey: survey.survey}
					, description: survey.description
					, email: email && email.to && email.to[0] 
				});
				transacter.charge({
					amount: survey.amount
					, token: survey.token
					, metadata: {survey: survey.survey}
					, description: survey.description
					, email: email && email.to && email.to[0] 
				}, null, (err, resp) => {
					if(err) return next(err);
					survey.transaction = resp;
					next();
				});
			} catch(ex) {
				next(ex);
			}
		}
		
		function saveAndSend(next) {
			survey.ip = 'ip ' + req.ip;
			const options = {
				survey, email, requestDomain 
			}
			surveyer.saveAndSend(options, null, (err) => {
				if(err) return next(err);
				next(null, {url: survey.redirecturl})
			});	
		}
		
		async.waterfall([
			parseForm
			, getAmount
			, purchase
			, saveAndSend
		], (err, resp) => {
			console.log(err)
			if(err) return next({status: 500, error: err});
			return res.send({url: resp.url});
		})
	})

}

module.exports = {
	addRoutes, initialize
}