let $helpBtn = document.querySelector('.button-help i');
let $helpBtnSubmenus = document.querySelector('.button-help-submenus');
let $helpSubmenusBackground = document.querySelector('.button-help-submenus-background');

function openHelpButtonSubmenus() {
	$helpBtnSubmenus.style.display = 'block';
	$helpSubmenusBackground.style.display = 'block';
}

function closeHelpButtonSubmenus() {
	$helpBtnSubmenus.style.display = 'none';
	$helpSubmenusBackground.style.display = 'none';
}

async function bindHelpForm() {
	const {injectHelpFormOverlayWindow} = await import('/libs/supporter/helpform/inject.js');
	const buttons = document.querySelectorAll('.contact-support-button');
	for (const button of buttons) {
		button.addEventListener('click', () => {
			closeHelpButtonSubmenus();
			injectHelpFormOverlayWindow(document.body);
		});
	}
}


$helpBtn.addEventListener('click', openHelpButtonSubmenus);
$helpSubmenusBackground.addEventListener('click', closeHelpButtonSubmenus);
bindHelpForm().then;