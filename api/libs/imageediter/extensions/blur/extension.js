import Extension from "../extension.js";
import init from "./wasm/image_blur.js";
import { blur } from "./wasm/image_blur.js";
const config = {
    author: "Tong Miao",
    email: "tonymiaotong@tioft.tech",
    domain: "tongmiao.cloud",
    name: "blur",
    description: "An extension to blur image",
    version: "0.0.1",
    buttonIcon: "blur/buttonicon.svg",
    settings: "blur/settings.html",
};
class BlurExtension extends Extension {
    async onload($container, options) {
        await super.onload($container, options);
        await init();
    }
    imageclick(e) {
        if (!super.imageclick(e))
            return false;
        const ctx = this.$canvas.getContext("2d");
        if (!ctx)
            return true;
        const imageData = ctx.getImageData(0, 0, this.$canvas.width, this.$canvas.height);
        const radius = 17;
        const start = performance.now();
        blur(imageData.data, imageData.data.length, 17, imageData.width);
        const end = performance.now();
        console.log("time:", (end - start) / 1000, "sec");
        console.log("size:", imageData.data.length);
        ctx.putImageData(imageData, 0, 0);
        return true;
    }
    select(e) {
        super.select(e);
    }
}
export default new BlurExtension(config);
