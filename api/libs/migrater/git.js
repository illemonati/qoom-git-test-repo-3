let pushing = false;

const parseCookie = (str) =>
    str
        .split(';')
        .map((v) => v.split('='))
        .reduce((acc, v) => {
            acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(
                v[1].trim()
            );
            return acc;
        }, {});

const connectToSocket = async (id) => {
    const textArea = document.querySelector('.migrater-git-outputs')
    const pushToGitButton = document.querySelector('.migrater-git-push-button')
    const pullFromGitButton = document.querySelector('.migrater-git-pull-button')
    pushToGitButton.disabled = true;
    pullFromGitButton.disabled = true;
    pushing = true;
    const socket = io('/migrate/git-connect-socket/' + id);
    socket.on('connect', () => {
        console.log('Socket Connected');
        textArea.value += `\n\n------ ${new Date()} ------\n`;
        textArea.scrollTop = textArea.scrollHeight;
        socket.emit('join');
        socket.emit('auth', parseCookie(document.cookie)['passcode']);
        socket.emit('execute');
    });
    socket.on('status', (msg) => {
        console.log(msg);
        textArea.value += msg + '\n';
        textArea.scrollTop = textArea.scrollHeight;
    });
    socket.on('disconnect', () => {
        console.log('Socket Disconnected');
        textArea.value += `\n\n-----------------------------------------------------------------------\n`;
        textArea.scrollTop = textArea.scrollHeight;
        pushToGitButton.disabled = false;
        pullFromGitButton.disabled = false;
        pushing = false;
    });
};


export const removePastGitConfig = async (options) => {
	console.log(options)
	await fetch('/migrate/remove-past-git-config', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			id: options._id
		})
	})
}

export const toggleButtons = async () => {
	console.log(1)
    const pushToGitButton = document.querySelector('.migrater-git-push-button');
	const pullFromGitButton = document.querySelector('.migrater-git-pull-button');
	if (document.querySelector('.migrater-git-auth-token').value &&
		document.querySelector('.migrater-git-url').value &&
		document.querySelector('.migrater-git-username').value &&
		!pushing
	){
	    pushToGitButton.disabled = false;
    	pullFromGitButton.disabled = false;
	} else {
	    pushToGitButton.disabled = true;
    	pullFromGitButton.disabled = true;
	}
}

export const refreshPastGitConfigs = async () => {
	const pastConnectionsContainer = document.querySelector('.migrater-git-past-connections-container');
	pastConnectionsContainer.innerHTML = '';
	const resp = await fetch('/migrate/past-git-configs', {
        method: 'POST',
    });
    const pastGitConfigs = await resp.json();
    for (const config of pastGitConfigs) {
    	// console.log(config);
    	pastConnectionsContainer.innerHTML += `
	    	<div class="migrater-git-past-connection">
				<div class="migrater-git-past-connection-auth-container">
					<p>Token:    ${config.token}</p>
					<p>Username: ${config.username}</p>
					<p>URL:      ${config.url}</p>
				</div>
				<div class="migrater-git-past-connection-buttons-container">
					<div class="migrater-git-past-connection-button-container">
					<div class="migrater-git-past-connection-button migrater-git-past-connection-select-button qoom-main-btn qoom-button-full qoom-button-small" data-config=${JSON.stringify(config)}>Select
					</div>
					</div>
					<div class="migrater-git-past-connection-button-container">
						<div class="migrater-git-past-connection-button migrater-git-past-connection-remove-button qoom-main-btn qoom-button-full qoom-button-small" data-config=${JSON.stringify(config)}>Remove
						</div>
					</div>
				</div>
			</div>
    	`.trim();
    }
    console.log(pastGitConfigs);
    if (pastGitConfigs.length === 0) {
    	pastConnectionsContainer.innerText = 'No past connections yet'
    }
    for (const button of document.querySelectorAll('.migrater-git-past-connection-select-button')) {
    	// console.log(button);
    	button.onclick = () => {
    		const config = JSON.parse(button.getAttribute('data-config'));
    		document.querySelector('.migrater-git-auth-token').value = config.token;
    		document.querySelector('.migrater-git-url').value = config.url;
    		document.querySelector('.migrater-git-username').value = config.username;
    		showPastGitConfigs();
    		toggleButtons();
    	}
    }
    for (const button of document.querySelectorAll('.migrater-git-past-connection-remove-button')) {
    	// console.log(button);
    	button.onclick = async () => {
    		const config = JSON.parse(button.getAttribute('data-config'));
    		await removePastGitConfig(config);
    		await refreshPastGitConfigs();
    	}
    }
}

export const showPastGitConfigs = async () => {
	const root = document.documentElement;
	const arrowRotate = root.style.getPropertyValue('--git-arrow-rotate') || '180deg';
	root.style.setProperty('--git-arrow-rotate', (arrowRotate === '180deg') ? '0deg' : '180deg'); 
	// root.style.setProperty('--git-connect-display', (arrowRotate === '180deg') ? 'none' : 'block'); 
	root.style.setProperty('--git-past-connections-display', (arrowRotate === '180deg') ? 'block' : 'none'); 
	if (arrowRotate === '180deg') {
		await refreshPastGitConfigs();
	}
}




export const checkForDisabled = async () => {
	for (const input of [
			document.querySelector('.migrater-git-auth-token'), 
			document.querySelector('.migrater-git-url'),
			document.querySelector('.migrater-git-username')
	]){
		input.addEventListener('input', toggleButtons);	
		input.addEventListener('change', toggleButtons);
	}
	toggleButtons();
}


export const connectToGit = async (command) => {
    console.log(`connectToGit: ${command}`);
    const accessToken = document.querySelector('.migrater-git-auth-token').value.trim();
    const gitURL = document.querySelector('.migrater-git-url').value.trim();
    const gitUserName = document.querySelector('.migrater-git-username').value.trim();
    
    document.documentElement.style.setProperty('--git-outputs-display', 'flex');
    
    const gitToMaster = true;
    const pushToGitButton = document.querySelector('.migrater-git-push-button')
    const pullFromGitButton = document.querySelector('.migrater-git-pull-button')
    pushToGitButton.disabled = true;
    pullFromGitButton.disabled = true;
    const resp = await fetch('/migrate/connect-to-git', {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
        body: JSON.stringify({
            accessToken,
            gitUserName,
            gitURL,
            gitToMaster,
            command,
        }),
    });

    const body = await resp.json();
    const id = body.id;
    connectToSocket(id);
};