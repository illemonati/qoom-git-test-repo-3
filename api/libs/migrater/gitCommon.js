const parseCookie = (str) =>
    str
        .split(';')
        .map((v) => v.split('='))
        .reduce((acc, v) => {
            acc[decodeURIComponent(v[0].trim())] = decodeURIComponent(
                v[1].trim()
            );
            return acc;
        }, {});

const connectToSocket = async (id, callbacks) => {
	const {connectCallback, statusCallback, disconnectCallback} = callbacks;
    const socket = io('/migrate/git-connect-socket/' + id);
    socket.on('connect', () => {
    	connectCallback && connectCallback();
        socket.emit('join');
        socket.emit('auth', parseCookie(document.cookie)['passcode']);
        socket.emit('execute');
    });
    socket.on('status', (msg) => {
        statusCallback && statusCallback(msg);
    });
    socket.on('disconnect', () => {
        disconnectCallback && disconnectCallback();
    });
};



export const connectToGit = async (command, options, callbacks) => {
    console.log(`connectToGit: ${command}`);
    const {accessToken, gitURL, gitUserName, directory} = options;
    const gitToMaster = true;
    const resp = await fetch('/migrate/connect-to-git', {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/json',
        }),
        body: JSON.stringify({
            accessToken,
            gitUserName,
            gitURL,
            gitToMaster,
            directory,
            command,
        }),
    });

    const body = await resp.json();
    const id = body.id;
    connectToSocket(id, callbacks);
};