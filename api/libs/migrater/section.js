import download from '/libs/migrater/download.js';
import {connectToGit, showPastGitConfigs, checkForDisabled} from '/libs/migrater/git.js';

export const main = async () => {
	document.querySelector('.migrater-download-button').addEventListener('click', download);
	document.querySelector('.migrater-git-pull-button').addEventListener('click', () => connectToGit('pull'));
	document.querySelector('.migrater-git-push-button').addEventListener('click', () => connectToGit('push'));
	document.querySelector('.migrater-git-show-past-connections').addEventListener('click', showPastGitConfigs);
	checkForDisabled();
}


main().then();