import {states} from '/libs/supporter/helpform/utils.js';



const submitForm = async () => {
	const form = new FormData();
	form.append('stars', states.stars);
	form.append('catagory', states.catagory);
	form.append('experience', states.experience);
	form.append('survey', 'Help Form');
	for (const status of states.fileStatuses) {
		const file = status.file;
		form.append(file.name, file, `${states.tempId}-${file.name}`);
	}
	
	const xhr = new XMLHttpRequest();
	xhr.upload.onprogress = (e) => {
		console.log((100 * e.loaded) / e.total);
	}
	xhr.onreadystatechange = () => {
		if (xhr.readyState === 4) {
			console.log('done');
		}
	}
	xhr.open('POST', '/survey/checkout');
	xhr.send(form);
	
	return false;
}

export {
	submitForm
}