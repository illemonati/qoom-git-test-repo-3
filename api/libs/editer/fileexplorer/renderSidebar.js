import {states, getFolderFiles, fileToFilepath, processFolderPath} from '/libs/editer/fileexplorer/utils.js';
import handleFileChange from '/libs/editer/fileexplorer/handleFileChange.js';
import createContextMenu from '/libs/editer/fileexplorer/createContextMenu.js';
import {setUpDragAndDrop} from '/libs/uploader/noModalUploaderBinder/noModalUploaderBinder.js';




export const renderFolder = async (fileInfo, element, folderstructure, layer=0, prevFolderPath='/', findOptions=states.findOptions, amountRenderedCallback, removeSelf=false) => {
	let amountRendered = 0;
	for (const folderName in folderstructure.folders) {
		const folderDiv = document.createElement('div');
		const currentFolderPath = prevFolderPath + '/' + folderName;
		folderDiv.classList.add('fileexplorer-files-folder');
		
		if (states.folderStates[currentFolderPath] === null || states.folderStates[currentFolderPath] === undefined) {
			states.folderStates[currentFolderPath] = false;
		}
		if (findOptions.query && (states.findFolderStates[currentFolderPath] === null || states.findFolderStates[currentFolderPath] === undefined)) {
			states.findFolderStates[currentFolderPath] = true;	
		}
		
		
		folderDiv.innerHTML = `
			<p class="fileexplorer-files-folder-text no-select">${folderName}</p>
			<div class="fileexplorer-files-folder-open-indicator-container">
				<i class="${(findOptions.query ? states.findFolderStates[currentFolderPath] : states.folderStates[currentFolderPath]) ? 'ic-chevron-down' : 'ic-chevron-up'} fileexplorer-files-folder-open-indicator"></i>
			</div>
		`.trim();
		if (!removeSelf) element.appendChild(folderDiv);
		folderDiv.style.paddingLeft = `calc(${window.getComputedStyle(folderDiv).getPropertyValue('padding-left') || '0em'} + ${layer}em)`;
		folderDiv.addEventListener('click', () => {
			if (!findOptions.query) {
				states.folderStates[currentFolderPath] = !states.folderStates[currentFolderPath];
			} else {
				states.findFolderStates[currentFolderPath] = !states.findFolderStates[currentFolderPath];
				console.log(states.findFolderStates[currentFolderPath]);
			}
			renderSidebar(fileInfo);
		})
		folderDiv.addEventListener('contextmenu', e => {
			const folderInfo = {
				folderName: folderName,
				folderStructure: folderstructure.folders[folderName],
				folderPath: currentFolderPath,
			};
			createContextMenu(e, null, folderInfo);
		})
		const folderAmountRenderedCallback = (n) => {
			if ((n < 1) && (findOptions.query)) {
				folderDiv.remove();
			} else {
				amountRendered ++;
			}
		}
		setUpDragAndDrop(folderDiv, () => currentFolderPath, true);
		// console.log(states.findFolderStates);
		if (findOptions.query || states.folderStates[currentFolderPath]) {
			await renderFolder(fileInfo, element, folderstructure.folders[folderName], layer+1, currentFolderPath, findOptions, folderAmountRenderedCallback, (findOptions.query && !states.findFolderStates[currentFolderPath]) || removeSelf);
		}
	}
	
	folderstructure.files.sort((a, b) => a.name.localeCompare(b.name));
	
	for (const file of folderstructure.files) {
		if (file.name === '__hidden') continue;
		// console.log(findOptions);
		if (findOptions.query && !findOptions.query(file)) continue;
		// console.log(file);
		amountRendered++;
		const fileDiv = document.createElement('div');
		fileDiv.classList.add('fileexplorer-files-file');
		fileDiv.innerHTML = `<p class="no-select">${file.name}</p>`.trim();
		(fileInfo.filePath === fileToFilepath(file)) && (fileDiv.classList.add('fileexplorer-files-selected-file'));
		fileDiv.addEventListener('click', () => {
			const newFileInfo = {
				...fileInfo,
				filePath: fileToFilepath(file),
				fileName: file.name,
				folderPath: file.directory || ''
			};
			handleFileChange(newFileInfo);
		});
		// console.log(fileDiv.style.paddingLeft);
		if (!removeSelf) element.appendChild(fileDiv);
		// console.log(window.getComputedStyle(fileDiv).getPropertyValue('padding-left'));
		fileDiv.style.paddingLeft = `calc(${window.getComputedStyle(fileDiv).getPropertyValue('padding-left') || '0em'} + ${layer}em)`;
		fileDiv.addEventListener('contextmenu', (e) => {
			createContextMenu(e, file);
		})
	}
	amountRenderedCallback && amountRenderedCallback(amountRendered);
}

const renderSidebar = async (fileInfo=states.currentFileInfo, refreshStructure=false, findOptions=states.findOptions) => {
	const folderstructure = (!states.pastFolderStructure || refreshStructure) ? await getFolderFiles(states.ogFileInfo) : states.pastFolderStructure;
	states.pastFolderStructure = folderstructure;
	states.currentFileInfo = fileInfo;
	const fileexplorerFilesDivs = document.getElementsByClassName('fileexplorer-files');
	for (const div of fileexplorerFilesDivs) {
		div.innerHTML = '';
		const rootFolderPath = processFolderPath(states.ogFileInfo.folderPath);
		div.addEventListener('contextmenu', e => {
			const folderInfo = {
				folderName: rootFolderPath.substring(rootFolderPath.lastIndexOf('/')+1),
				folderStructure: folderstructure,
				folderPath: rootFolderPath,
			};
			createContextMenu(e, null, folderInfo);
		});
		renderFolder(fileInfo, div, folderstructure, 0, rootFolderPath, findOptions);
	}
}


export default renderSidebar;