import renderSidebar from '/libs/editer/fileexplorer/renderSidebar.js';
import {states, fileToFilepath} from '/libs/editer/fileexplorer/utils.js';


const setUpFind = async () => {
	const findInput = document.querySelector('.fileexplorer-find-input');
	const findRegexCheckbox = document.querySelector('.fileexplorer-find-regex-checkbox');
	const findPathCheckbox = document.querySelector('.fileexplorer-find-path-checkbox');
	findInput.value = "";
	findRegexCheckbox.checked = false;	
	findPathCheckbox.checked = false;
	const refreshFind = async () => {
		const input = findInput.value;
		const usePath = findPathCheckbox.checked;
		const useRegex = findRegexCheckbox.checked;
		const findOptions = {
			query: (input && ((file) => {
				const stringToCheck = usePath ? fileToFilepath(file) : file.name;
				try {
					if (useRegex) {
						const flags = input.replace(/.*\/([gimy]*)$/, '$1');
						const pattern = input.replace(new RegExp('^/(.*?)/'+flags+'||DATA||#39;'), '$1');
						const re = new RegExp(pattern, flags);
						return re.test(stringToCheck)
					} else {
						return stringToCheck.includes(input);
					}
				} catch (e) {
					return false;
				}
			}))
		}
		states.findOptions = findOptions;
		states.findFolderStates = {};
		await renderSidebar(undefined, undefined, findOptions);
	}
	findInput.addEventListener('input', refreshFind);
	findRegexCheckbox.addEventListener('change', refreshFind);
	findPathCheckbox.addEventListener('change', refreshFind);
}


const setUpSidebarToggle = async () => {
	const sideBarToggleButton = document.querySelector('.fileexplorer-editor-toggle-sidebar-button');
	const sideBar = document.querySelector('.fileexplorer-sidebar');
	const ogWidth = sideBar.style.width;
	const ogMinWidth = sideBar.style.minWidth;
	const icons = {
		open: 'ic-chevron-left',
		closed: 'ic-chevron-right'
	}
	sideBarToggleButton.addEventListener('click', async e => {
		const sideBarHidden = sideBar.style.width === '0px';
		if (sideBarHidden) {
			sideBar.style.width = ogWidth;
			sideBar.style.minWidth = ogMinWidth;
			sideBarToggleButton.classList.remove(icons.closed);
			sideBarToggleButton.classList.add(icons.open);
		} else {
			sideBar.style.width = '0px';
			sideBar.style.minWidth = '0px';
			sideBarToggleButton.classList.remove(icons.open);
			sideBarToggleButton.classList.add(icons.closed);
		}
	});
}

export {setUpFind, setUpSidebarToggle};