import handleFileChange from '/libs/editer/fileexplorer/handleFileChange.js';
import {states} from '/libs/editer/fileexplorer/utils.js';
import {setUpFind, setUpSidebarToggle} from '/libs/editer/fileexplorer/uiBindings.js';

const main = async (startingFileInfo) => {
	states.ogFileInfo = startingFileInfo;
	await Promise.all([
		handleFileChange(startingFileInfo),
		setUpSidebarToggle(), 
		setUpFind(), 
	]);
}

export {
	main
}