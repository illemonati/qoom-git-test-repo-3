class Modal {
	
	constructor(options) {
		const { modalContainerId, modalTitleText, modalTitlePosition, modalContentsInnerHTML, modalCancelBtnText, modalSubmitBtnText, modalCancelBtnAction, modalSubmitBtnAction } = options;
		
		this.$modaler = undefined;
		this.modalContainerId = modalContainerId;
		this.modalTitleText = modalTitleText || '';
		this.modalTitlePosition = modalTitlePosition || 'left';
		this.modalContentsInnerHTML = modalContentsInnerHTML;
		this.modalCancelBtnText = modalCancelBtnText || '';
		this.modalSubmitBtnText = modalSubmitBtnText || '';
		this.modalCancelBtnAction = modalCancelBtnAction || undefined;
		this.modalSubmitBtnAction = modalSubmitBtnAction || undefined;
		if(!this.modalContainerId) return;
		if(!this.modalContentsInnerHTML) return;
	}
	
	show() {
		if(!this.modalContentsInnerHTML) return;
		const self = this;
		//make trial modal
		this.$modaler = document.createElement('div');
		this.$modaler.id = this.modalContainerId;
		if(!this.modalContainerId) return;
		const $modalBackground = document.createElement('div');
		$modalBackground.className = 'modal-background';
		
		if(!!this.modalCancelBtnAction) {
			$modalBackground.onclick = this.modalCancelBtnAction;
		}

		this.$modaler.appendChild($modalBackground);
		
		const $modal = document.createElement('div');
		$modal.className = 'modal';
		const $modalTitle = document.createElement('div');
		$modalTitle.className = 'modal-title';
		const $modalTitleH = document.createElement('h1');
		$modalTitleH.innerText = this.modalTitleText;
		$modalTitle.appendChild($modalTitleH);
		$modal.appendChild($modalTitle); 
		
		const $modalContentsContainer = document.createElement('div');
		$modalContentsContainer.className = 'container';
		$modalContentsContainer.innerHTML = this.modalContentsInnerHTML;
		
		if(!!this.modalCancelBtnText || !!this.modalSubmitBtnText) {
			const $modalBtnsContainer = document.createElement('div');
			$modalBtnsContainer.className = 'buttons-container';
			if(!!this.modalCancelBtnText ) {
				const $modalCancelBtn = document.createElement('button');
				$modalCancelBtn.className = 'qoom-main-btn qoom-button-outline qoom-button-small';
				$modalCancelBtn.setAttribute('type', 'cancel');
			    $modalCancelBtn.innerText = this.modalCancelBtnText;
			    $modalCancelBtn.onclick = this.modalCancelBtnAction;
			    $modalBtnsContainer.appendChild($modalCancelBtn);
			}	
			if(!!this.modalSubmitBtnText) {
				const $modalSubmitBtn = document.createElement('button');
				$modalSubmitBtn.className = 'qoom-main-btn qoom-button-full qoom-button-small';
				$modalSubmitBtn.id = 'submitBtn';
			    $modalSubmitBtn.setAttribute('type', 'submit');
			    $modalSubmitBtn.innerText = this.modalSubmitBtnText;
			    $modalSubmitBtn.onclick = this.modalSubmitBtnAction;
			    $modalBtnsContainer.appendChild($modalSubmitBtn);
			}
			$modalContentsContainer.appendChild($modalBtnsContainer);
		}
		
		$modal.appendChild($modalContentsContainer);
		this.$modaler.appendChild($modal);
		document.body.appendChild(this.$modaler);
		
	}
	
	destroy(){
		document.body.removeChild(this.$modaler);
		delete this.$modaler;
	}
}    

export default Modal